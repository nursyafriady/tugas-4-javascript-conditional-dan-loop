// Soal 1
var nilai = 75;

if (nilai >= 85) {
  console.log("indeksnya A");
} else if (nilai >= 75) {
  console.log("indeksnya B");
} else if (nilai >= 65) {
  console.log("indeksnya C");
} else if (nilai >= 55) {
  console.log("indeksnya D");
} else {
  console.log("indeksnya E");
}

// Soal 2
var tanggal = 5;
var bulan = 3;
var tahun = 1983;

switch (bulan) {
  case 1: {
    console.log(tanggal + " Januari " + tahun);
    break;
  }
  case 2:
    console.log(tanggal + " Februari " + tahun);
    break;
  case 3:
    console.log(tanggal + " Maret " + tahun);
    break;
  case 4:
    console.log(tanggal + " April " + tahun);
    break;
  case 5:
    console.log(tanggal + " Mei " + tahun);
    break;
  case 6:
    console.log(tanggal + " Juni " + tahun);
    break;
  case 7:
    console.log(tanggal + " Juli " + tahun);
    break;
  case 8:
    console.log(tanggal + " Agustus " + tahun);
    break;
  case 9:
    console.log(tanggal + " September " + tahun);
    break;
  case 10:
    console.log(tanggal + " Oktober " + tahun);
    break;
  case 11:
    console.log(tanggal + " November " + tahun);
    break;
  case 12:
    console.log(tanggal + " Desember " + tahun);
    break;
  default:
    console.log("Tanggal Lahir Tidak Ditemukan");
}

// Soal 3
var n = 3;
var segitiga = "";
for (var i = 0; i < n; i++) {
  for (var j = 0; j <= i; j++) {
    segitiga += "#";
  }
  segitiga += "\n";
}

console.log(segitiga);

var n = 7;
var segitiga = "";
for (var i = 0; i < n; i++) {
  for (var j = 0; j <= i; j++) {
    segitiga += "#";
  }
  segitiga += "\n";
}

console.log(segitiga);

// Soal 4
var barisBaru = "\n";
console.log("m=3");
var m = 3;
for (var i = 1; i <= m; i++) {
  switch (i) {
    case 1: {
      console.log(i + " - I love programming");
      break;
    }
    case 2: {
      console.log(i + " - I love Javascript");
      break;
    }
    case 3: {
      console.log(i + " - I love VueJS");
      break;
    }
    default:
      console.log("tidak ada");
  }
}
console.log("===");
console.log(barisBaru);

console.log("m=5");
var m = 5;
for (var i = 1; i <= m; i++) {
  switch (i) {
    case 1: {
      console.log(i + " - I love programming");
      break;
    }
    case 2: {
      console.log(i + " - I love Javascript");
      break;
    }
    case 3: {
      console.log(i + " - I love VueJS");
      console.log("===");
      break;
    }
    case 4: {
      console.log(i + " - I love programming");
      break;
    }
    case 5: {
      console.log(i + " - I love Javascript");
      break;
    }
    default:
      console.log("tidak ada");
  }
}
console.log(barisBaru);

console.log("m=7");
var m = 7;
for (var i = 1; i <= m; i++) {
  switch (i) {
    case 1: {
      console.log(i + " - I love programming");
      break;
    }
    case 2: {
      console.log(i + " - I love Javascript");
      break;
    }
    case 3: {
      console.log(i + " - I love VueJS");
      console.log("===");
      break;
    }
    case 4: {
      console.log(i + " - I love programming");
      break;
    }
    case 5: {
      console.log(i + " - I love Javascript");
      break;
    }
    case 6: {
      console.log(i + " - I love VueJS");
      console.log("===");
      break;
    }
    case 7: {
      console.log(i + " - I love programming");
      break;
    }
    default:
      console.log("tidak ada");
  }
}
console.log(barisBaru);

console.log("m=10");
var m = 10;
for (var i = 1; i <= m; i++) {
  switch (i) {
    case 1: {
      console.log(i + " - I love programming");
      break;
    }
    case 2: {
      console.log(i + " - I love Javascript");
      break;
    }
    case 3: {
      console.log(i + " - I love VueJS");
      console.log("===");
      break;
    }
    case 4: {
      console.log(i + " - I love programming");
      break;
    }
    case 5: {
      console.log(i + " - I love Javascript");
      break;
    }
    case 6: {
      console.log(i + " - I love VueJS");
      console.log("===");
      break;
    }
    case 7: {
      console.log(i + " - I love programming");
      break;
    }
    case 8: {
      console.log(i + " - I love Javascript");
      break;
    }
    case 9: {
      console.log(i + " - I love VueJS");
      console.log("===");
      break;
    }
    case 10: {
      console.log(i + " - I love programming");
      break;
    }
    default:
      console.log("tidak ada");
  }
}
